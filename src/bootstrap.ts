import { env } from 'process';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import serverlessExpress from '@codegenie/serverless-express';
import { CommonLoggerService } from '@roomee1/common';
import { DomainExceptionFilter } from './middlewares';
import {
  I18nValidationException,
  I18nValidationExceptionFilter,
} from 'nestjs-i18n';

let cacheServer = null;
export type BootstrapOptions = {
  enableSwagger: boolean;
};
export const validationError = (errors) => {
  return new I18nValidationException(errors);
};

export const setupSwagger = (app: INestApplication) => {
  const environment = env.NODE_ENV ?? '';
  const swaggerConfig = new DocumentBuilder()
    .setTitle('Authentication')
    .setDescription('The Authentication API description')
    .setVersion('1.0')
    .addBearerAuth()
    .addServer(`/${environment}`)
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document, {
    swaggerOptions: { defaultModelsExpandDepth: -1 },
  });
};

export async function bootstrap(options?: BootstrapOptions) {
  if (!cacheServer) {
    const app = await NestFactory.create(AppModule, {
      cors: true,
      logger: false,
    });

    const logger = await app.resolve(CommonLoggerService);
    app.useLogger(logger);
    app.useGlobalFilters(
      new DomainExceptionFilter(logger),
      new I18nValidationExceptionFilter(),
    );
    app.useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: validationError,
      }),
    );

    if (options?.enableSwagger) setupSwagger(app);
    await app.init();

    const expressApp = app.getHttpAdapter().getInstance();
    cacheServer = serverlessExpress({ app: expressApp });
  }

  return cacheServer;
}
