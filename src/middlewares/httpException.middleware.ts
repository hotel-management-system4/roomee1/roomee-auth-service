import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { CommonLoggerService } from '@roomee1/common';
import { Response } from 'express';
import { I18nContext } from 'nestjs-i18n';
@Catch(HttpException)
export class DomainExceptionFilter implements ExceptionFilter {
  constructor(private logger: CommonLoggerService) {
    logger.setContext(DomainExceptionFilter.name);
  }
  catch(exception: HttpException, host: ArgumentsHost) {
    const i18n = I18nContext.current(host);
    const response = host.switchToHttp().getResponse<Response>();
    this.logger.info(`exception: ${JSON.stringify(exception)}`);

    const cause = exception.cause || 'internal_server_error';
    const status = exception.getStatus() || 500;
    return response.status(status).json({
      message: i18n.t(`error.${cause}`),
      error: exception.cause,
    });
  }
}
