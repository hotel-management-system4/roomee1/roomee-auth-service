import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ForbiddenException, UnauthorizedException } from '@roomee1/common';
import { Request } from 'express';
import { TokenOptions, UserData } from 'types';

@Injectable()
export class JwtAccessGuard implements CanActivate {
  private readonly accessTokenOptions: TokenOptions;
  constructor(
    configService: ConfigService,
    private jwtService: JwtService,
  ) {
    this.accessTokenOptions = configService.get<TokenOptions>('accessToken');
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    const authorization = request.headers['authorization'];
    if (!authorization) {
      throw new UnauthorizedException('Token not appear in header', {
        cause: 'not_appear_token',
      });
    }

    const token = authorization.split(' ')[1];
    if (!token) {
      throw new UnauthorizedException('Not supported token strategy', {
        cause: 'not_supported_token',
      });
    }

    const user = await this.jwtService.verifyAsync<UserData>(token, {
      publicKey: this.accessTokenOptions.publicKey,
    });

    if (!user) {
      return false;
    }

    if (user.banned) {
      throw new ForbiddenException('This account is banned', {
        cause: 'banned_account',
      });
    }

    // if (!user.emailVerified) {
    //   throw new ForbiddenException(
    //     `This account's email hasn't been verified yet`,
    //     { cause: 'not_verified_account' },
    //   );
    // }

    delete user['iat'];
    delete user['exp'];
    delete user['iss'];
    delete user['sub'];
    request.user = user;

    return true;
  }
}
