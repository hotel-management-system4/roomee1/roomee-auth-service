export const selectUserFragment = {
  id: true,
  username: true,
  emailVerified: true,
  phoneNumber: true,
  phoneVerified: true,
  role: {
    select: {
      name: true,
    },
  },
  firstName: true,
  lastName: true,
  birthday: true,
  imageUrl: true,
  banned: true,
  locked: true
};
