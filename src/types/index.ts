export interface UserData {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  imageUrl: string;
  phoneNumber: string;
  emailVerified: boolean;
  phoneVerified: boolean;
  role: string;
  birthday: string;
  banned: boolean;
  locked: boolean;
}

export interface DeviceData {
  deviceId: string;
  deviceName: string;
}
export interface TrackingData {
  trackingId: string;
}

export type FacebookOptions = {
  clientID: string;
  appSecret: string;
  callbackUrl: string;
  scope: string;
  profileFields: string[];
};

export type GoogleOptions = {
  clientID: string;
  appSecret: string;
  callbackUrl: string;
  scope: string;
};

export type TokenOptions = {
  privateKey: string;
  publicKey: string;
  expireTime: number;
};

export type CacheOptions = {
  url: string;
};
