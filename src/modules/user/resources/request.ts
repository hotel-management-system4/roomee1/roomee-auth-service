import { ApiProperty } from '@nestjs/swagger';
import { RoleList } from '@prisma/client';
import { defaultValue, parseInt } from '@roomee1/common';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsStrongPassword,
  ValidateNested,
} from 'class-validator';
import {
  i18nIsBoolean,
  i18nIsEmail,
  i18nIsNotEmpty,
  i18nIsNumber,
  i18nIsString,
} from 'utils/i18n.validation';

export class VerifyDTO {
  @ApiProperty({
    default: 1234,
    type: Number,
  })
  @IsNumber({ maxDecimalPlaces: 4 }, i18nIsNumber)
  verifyCode: number;
}

export class UpdateUserProfileDTO {
  @ApiProperty({
    default: 'firstname',
    type: String,
  })
  @IsString(i18nIsString)
  firstName: string;

  @ApiProperty({
    default: 'lastname',
    type: String,
  })
  @IsString(i18nIsString)
  lastName: string;

  @ApiProperty({
    default: 'birthday',
    type: String,
  })
  @IsString(i18nIsString)
  birthday: string;
}

export class UpdateUserAvatarDTO {
  buffer: Buffer;
  @IsString(i18nIsString)
  filename: string;
  mimeType: string;
}

export class ChangePasswordDTO {
  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsString(i18nIsString)
  @IsNotEmpty(i18nIsNotEmpty)
  password: string;
}

export class SharedCreateUserDTO {
  @ApiProperty({
    default: 'username',
    type: String,
  })
  @IsEmail({}, i18nIsEmail)
  username: string;

  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsString(i18nIsString)
  @IsNotEmpty(i18nIsNotEmpty)
  password: string;

  @ApiProperty({
    default: 'Firstname',
    type: String,
  })
  @IsString(i18nIsString)
  firstName: string;

  @ApiProperty({
    default: 'lastname',
    type: String,
  })
  @IsString(i18nIsString)
  lastName: string;

  @ApiProperty({
    default: 'TENANT',
    type: String,
  })
  @IsString(i18nIsString)
  role: RoleList;
}

export class AdminChangeUserRoleDTO {
  @ApiProperty({
    default: 'TENANT',
    type: String,
  })
  @IsString(i18nIsString)
  role: RoleList;
}

export class AdminBanUserDTO {
  @ApiProperty({
    default: true,
    type: Boolean,
  })
  @IsBoolean(i18nIsBoolean)
  ban: boolean;
}

export class AdminChangeUserPasswordDTO {
  @ApiProperty({
    default: 'password',
    type: String,
  })
  @IsString(i18nIsString)
  @IsNotEmpty(i18nIsNotEmpty)
  password: string;
}

export class AdminForceLogoutUserDTO {
  @ApiProperty({
    default: '[]',
    type: Array<String>,
  })
  @ValidateNested()
  @Type(() => String)
  deviceKeys: Array<string>;
}

export class FilterDTO {
  @ApiProperty({
    default: 10,
    type: Number,
  })
  @parseInt()
  @defaultValue(10)
  take: number;

  @ApiProperty({
    default: 0,
    type: Number,
  })
  @parseInt()
  @defaultValue(0)
  skip: number;
}

export class OwnerToggleLockUserDTO {
  @ApiProperty({
    default: true,
    type: Boolean,
  })
  @IsBoolean(i18nIsBoolean)
  locked: boolean;
}
