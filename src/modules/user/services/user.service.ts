import fs from 'fs';
import path from 'path';
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { IRedisCacheService, IScryptService } from '@roomee1/common';
import { ISendgridService } from '@roomee1/sendgrid';
import { IFirebaseStorageService } from '@roomee1/firebase';
import { randomInt, randomUUID } from 'crypto';
import { UserData } from 'types';
import { selectUserFragment } from 'types/fragment';
import { TrackingResponse, UserResponse } from '../resources/response';
import {
  ChangePasswordDTO,
  UpdateUserAvatarDTO,
  UpdateUserProfileDTO,
  VerifyDTO,
} from '../resources/request';

export const IUserService = 'IUserService';
export interface IUserService {
  getUserProfile(user: UserData): Promise<UserResponse>;
  verifyEmail(
    user: UserData,
    trackingId: string,
    verifyEmailDto: VerifyDTO,
  ): Promise<UserResponse>;
  verifyPhoneNumber(
    user: UserData,
    trackingId: string,
    verifyPhoneNumber: VerifyDTO,
  ): Promise<UserResponse>;
  sendVerifyEmailOTP(user: UserData): Promise<TrackingResponse>;
  resendVerifyEmailOTP(
    user: UserData,
    trackingId: string,
  ): Promise<TrackingResponse>;
  sendPhoneNumberOTP(user: UserData): Promise<TrackingResponse>;
  resendPhoneNumberOTP(
    user: UserData,
    trackingId: string,
  ): Promise<TrackingResponse>;
  updateUserProfile(
    user: UserData,
    updateUserProfileDto: UpdateUserProfileDTO,
  ): Promise<UserResponse>;
  updateUserAvatar(
    user: UserData,
    updateUserAvatarDto: UpdateUserAvatarDTO,
  ): Promise<UserResponse>;
  changePassword(
    user: UserData,
    changePasswordDto: ChangePasswordDTO,
  ): Promise<void>;
}

@Injectable()
export class UserService implements IUserService {
  constructor(
    private prismaService: PrismaService,
    @Inject(IRedisCacheService)
    private cacheService: IRedisCacheService,
    @Inject(IScryptService)
    private hashService: IScryptService,
    @Inject(IFirebaseStorageService)
    private firebaseStorage: IFirebaseStorageService,
    @Inject(ISendgridService)
    private sendgridService: ISendgridService,
  ) {}

  async getUserProfile(user: UserData): Promise<UserResponse> {
    const { role, ...payload } = await this.prismaService.user.findUnique({
      where: { id: user.id },
      select: selectUserFragment,
    });
    return { ...payload, role: role.name };
  }

  async verifyEmail(
    user: UserData,
    trackingId: string,
    verifyEmailDto: VerifyDTO,
  ): Promise<UserResponse> {
    const cacheKey = `${user.id}:tracking:${trackingId}`;
    const otp = await this.cacheService.get<string>(cacheKey);

    if (parseInt(otp) !== verifyEmailDto.verifyCode) {
      throw new BadRequestException('OTP not matched', { cause: 'wrong_otp' });
    }

    const userResp = await this.prismaService.user.update({
      where: {
        id: user.id,
      },
      data: {
        emailVerified: true,
      },
      select: {
        emailVerified: true,
      },
    });

    await this.cacheService.del(cacheKey);
    return {
      ...user,
      emailVerified: userResp.emailVerified,
    };
  }

  async verifyPhoneNumber(
    user: UserData,
    trackingId: string,
    verifyPhoneNumberDto: VerifyDTO,
  ): Promise<UserResponse> {
    const cacheKey = `${user.id}:tracking:${trackingId}`;
    const otp = await this.cacheService.get<string>(cacheKey);

    if (parseInt(otp) !== verifyPhoneNumberDto.verifyCode) {
      throw new BadRequestException('OTP not matched', { cause: 'wrong_otp' });
    }

    const userResp = await this.prismaService.user.update({
      where: {
        id: user.id,
      },
      data: {
        phoneVerified: true,
      },
      select: {
        phoneVerified: true,
      },
    });

    await this.cacheService.del(cacheKey);
    return {
      ...user,
      phoneVerified: userResp.phoneVerified,
    };
  }

  async sendVerifyEmailOTP(user: UserData): Promise<TrackingResponse> {
    if (user.emailVerified) {
      throw new BadRequestException('your account has been verified', {
        cause: 'verified_account',
      });
    }
    const { trackingId, otp } = this._randomOTP(4);
    const redisKey = `${user.id}:tracking:${trackingId}`;
    await this.cacheService.set(redisKey, otp, {
      EX: 300,
    });

    await this._sendVerifyEmail(user.username, otp);
    return {
      trackingId,
    };
  }

  async resendVerifyEmailOTP(
    user: UserData,
    trackingId: string,
  ): Promise<TrackingResponse> {
    const cacheKey = `${user.id}:tracking:${trackingId}`;
    await this.cacheService.del(cacheKey);
    const res = await this.sendVerifyEmailOTP(user);
    return res;
  }

  sendPhoneNumberOTP(user: UserData): Promise<TrackingResponse> {
    if (user.phoneVerified) {
      throw new BadRequestException('your account has been verified', {
        cause: 'verified_account',
      });
    }
    throw new Error('Method not implemented.');
  }

  async resendPhoneNumberOTP(
    user: UserData,
    trackingId: string,
  ): Promise<TrackingResponse> {
    const cacheKey = `${user.id}:tracking:${trackingId}`;
    await this.cacheService.del(cacheKey);
    const resp = await this.sendPhoneNumberOTP(user);
    return resp;
  }

  async updateUserProfile(
    user: UserData,
    updateUserProfileDto: UpdateUserProfileDTO,
  ): Promise<UserResponse> {
    const res = await this.prismaService.user.update({
      where: {
        id: user.id,
      },
      data: {
        firstName: updateUserProfileDto.firstName,
        lastName: updateUserProfileDto.lastName,
        birthday: updateUserProfileDto.birthday,
      },
      select: selectUserFragment,
    });

    return {
      ...res,
      role: res.role.name,
    };
  }

  async updateUserAvatar(
    user: UserData,
    updateUserAvatarDto: UpdateUserAvatarDTO,
  ): Promise<UserResponse> {
    const { imageUrl } = await this.prismaService.user.findUnique({
      where: {
        id: user.id,
      },
      select: {
        imageUrl: true,
      },
    });

    const oldImageUrl = imageUrl
      .replace(/%2F/, '/')
      .match(/avatar\/([A-Z0-9-._]+)/iu)?.[1];
    if (oldImageUrl) {
      await this.firebaseStorage.del(`avatar/${oldImageUrl}`);
    }

    const { buffer, filename } = updateUserAvatarDto;
    const cardBucket = `avatar/${user.id}-${filename}`;
    await this.firebaseStorage.upload(buffer, cardBucket);
    const url = await this.firebaseStorage.get(cardBucket);

    await this.prismaService.user.update({
      where: {
        id: user.id,
      },
      data: {
        imageUrl: url,
      },
      select: null,
    });

    return {
      ...user,
      imageUrl: url,
    };
  }

  async changePassword(
    user: UserData,
    changePasswordDto: ChangePasswordDTO,
  ): Promise<void> {
    const hashedPassword = await this.hashService.createHashedFromString(
      changePasswordDto.password,
    );
    await this.prismaService.user.update({
      where: {
        id: user.id,
      },
      data: {
        password: hashedPassword,
      },
      select: null,
    });
  }

  private _randomOTP(len: number) {
    const randomCode = randomInt(
      Math.pow(10, len - 1),
      9 * Math.pow(10, len - 1),
    );
    const trackingId = randomUUID();
    return {
      otp: randomCode,
      trackingId,
    };
  }

  private async _sendVerifyEmail(to: string, otp: number) {
    const bufferFile = await fs.promises.readFile(
      path.format({ root: 'src', base: '/templates/verifyEmail.html' }),
    );

    const file = bufferFile.toString();

    const formatHtml = file.replaceAll('[OTP]', otp.toString());
    await this.sendgridService.send({
      to,
      subject: 'verify your account',
      from: '20120551@student.hcmus.edu.vn',
      html: formatHtml,
    });
  }
}
