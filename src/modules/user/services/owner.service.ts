import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '@roomee1/prisma';
import { IRedisCacheService, IScryptService } from '@roomee1/common';
import { selectUserFragment } from 'types/fragment';
import { UserResponse } from '../resources/response';
import { FilterDTO, OwnerToggleLockUserDTO } from '../resources/request';
import { AdminOwnerSharedService, IAdminOwnerSharedService } from './shared';
import { RoleList } from '@prisma/client';

export const IOwnerService = 'IOwnerService';
export interface IOwnerService extends IAdminOwnerSharedService {
  getUsers(userId: string, filter: FilterDTO): Promise<UserResponse[]>;
  getUser(userId: string, managerId: string): Promise<UserResponse>;
  lockUser(
    userId: string,
    ownerToggleLockUserDto: OwnerToggleLockUserDTO,
  ): Promise<UserResponse>;
}

@Injectable()
export class OwnerService
  extends AdminOwnerSharedService
  implements IOwnerService
{
  constructor(
    protected prismaService: PrismaService,
    @Inject(IScryptService)
    protected scryptService: IScryptService,
    @Inject(IRedisCacheService)
    private redisCacheService: IRedisCacheService,
  ) {
    super(prismaService, scryptService);
  }

  isAllowCreatedAccount(role: string): boolean {
    return role === RoleList.MANAGER;
  }

  async lockUser(
    userId: string,
    ownerToggleLockUserDto: OwnerToggleLockUserDTO,
  ) {
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
      select: {
        locked: true,
      },
    });

    if (!user) {
      throw new BadRequestException('not found user', {
        cause: 'not_existed_user',
      });
    }

    if (user.locked && ownerToggleLockUserDto.locked) {
      throw new BadRequestException('user is locked', {
        cause: 'locked_account',
      });
    }

    if (!user.locked && !ownerToggleLockUserDto.locked) {
      throw new BadRequestException('user is unlocked', {
        cause: 'not_locked_account',
      });
    }

    const resp = await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        locked: ownerToggleLockUserDto.locked,
      },
      select: selectUserFragment,
    });

    return {
      ...resp,
      role: resp.role.name,
    };
  }

  async getUsers(userId: string, filter: FilterDTO): Promise<UserResponse[]> {
    const users = await this.prismaService.user.findMany({
      where: {
        createdBy: userId,
      },
      take: filter.take,
      skip: filter.skip,
      select: selectUserFragment,
    });

    const resp = users.map((user) => ({ ...user, role: user.role.name }));

    return resp;
  }

  async getUser(userId: string, managerId: string): Promise<UserResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
      select: selectUserFragment,
    });

    if (!user) {
      throw new BadRequestException('not found user', {
        cause: 'not_existed_user',
      });
    }

    return {
      ...user,
      role: user.role.name,
    };
  }
}
