import fs from 'fs';
import path from 'path';
import BPromise from 'bluebird';
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { ISendgridService } from '@roomee1/sendgrid';
import { PrismaService } from '@roomee1/prisma';
import { IRedisCacheService, IScryptService } from '@roomee1/common';
import { selectUserFragment } from 'types/fragment';
import { UserDeviceResponse, UserResponse } from '../resources/response';
import {
  AdminBanUserDTO,
  AdminChangeUserPasswordDTO,
  AdminChangeUserRoleDTO,
  AdminForceLogoutUserDTO,
  FilterDTO,
} from '../resources/request';
import { AdminOwnerSharedService, IAdminOwnerSharedService } from './shared';

export const IAdminService = 'IAdminService';
export interface IAdminService extends IAdminOwnerSharedService {
  getUsers(filter: FilterDTO): Promise<UserDeviceResponse[]>;
  getUser(id: string): Promise<UserDeviceResponse>;
  changeUserRole(
    id: string,
    adminChangeUserRoleDto: AdminChangeUserRoleDTO,
  ): Promise<UserResponse>;
  banUser(id: string, adminBanUserDto: AdminBanUserDTO): Promise<UserResponse>;
  changeUserPassword(
    id: string,
    adminChangeUserPassword: AdminChangeUserPasswordDTO,
  ): Promise<void>;
  forceLogout(adminForceLogoutDto: AdminForceLogoutUserDTO): Promise<void>;
}

@Injectable()
export class AdminService
  extends AdminOwnerSharedService
  implements IAdminService
{
  constructor(
    protected prismaService: PrismaService,
    @Inject(ISendgridService)
    private sendgridService: ISendgridService,
    @Inject(IScryptService)
    protected scryptService: IScryptService,
    @Inject(IRedisCacheService)
    private redisCacheService: IRedisCacheService,
  ) {
    super(prismaService, scryptService);
  }

  isAllowCreatedAccount(role: string): boolean {
    return true;
  }

  async forceLogout(
    adminForceLogoutDto: AdminForceLogoutUserDTO,
  ): Promise<void> {
    await BPromise.map(adminForceLogoutDto.deviceKeys, async (key) =>
      this.redisCacheService.del(key),
    );
  }

  async getUsers(filter: FilterDTO): Promise<UserDeviceResponse[]> {
    const users = await this.prismaService.user.findMany({
      take: filter.take,
      skip: filter.skip,
      select: selectUserFragment,
    });

    const resp = await BPromise.map(users, async ({ role, ...payload }) => {
      const devices = await this.redisCacheService.keys(
        `${payload.id}:device:*`,
      );
      return {
        user: {
          ...payload,
          role: role.name,
        },
        devices: devices.map((device) => ({
          key: device,
          deviceName: device.split(':').slice(-1)[0],
        })),
      };
    });

    return resp;
  }

  async getUser(id: string): Promise<UserDeviceResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        id,
      },
      select: selectUserFragment,
    });

    if (!user) {
      throw new BadRequestException('not found user', {
        cause: 'not_existed_user',
      });
    }

    const devices = await this.redisCacheService.keys(`${user.id}:device:*`);

    return {
      user: {
        ...user,
        role: user.role.name,
      },
      devices: devices.map((device) => ({
        key: device,
        deviceName: device.split(':').slice(-1)[0],
      })),
    };
  }

  async changeUserRole(
    id: string,
    adminChangeUserRoleDto: AdminChangeUserRoleDTO,
  ): Promise<UserResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        id,
      },
      select: {
        role: {
          select: {
            name: true,
          },
        },
      },
    });

    if (!user) {
      throw new BadRequestException('not found user', {
        cause: 'not_existed_user',
      });
    }

    if (adminChangeUserRoleDto.role === user.role.name) {
      throw new BadRequestException('both role are the same', {
        cause: 'same_role',
      });
    }

    const role = await this.prismaService.role.findUnique({
      where: {
        name: adminChangeUserRoleDto.role,
      },
      select: {
        id: true,
      },
    });

    if (!role) {
      throw new BadRequestException('unsupported role', {
        cause: 'not_supported_role',
      });
    }

    const resp = await this.prismaService.user.update({
      where: {
        id,
      },
      data: {
        roleId: role.id,
      },
      select: selectUserFragment,
    });

    return {
      ...resp,
      role: resp.role.name,
    };
  }

  async banUser(
    id: string,
    adminBanUserDto: AdminBanUserDTO,
  ): Promise<UserResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        id,
      },
      select: {
        banned: true,
      },
    });

    if (!user) {
      throw new BadRequestException('not found user', {
        cause: 'not_existed_user',
      });
    }

    if (user.banned && adminBanUserDto.ban) {
      throw new BadRequestException('user is banned', {
        cause: 'banned_account',
      });
    }

    if (!user.banned && !adminBanUserDto.ban) {
      throw new BadRequestException('user is unbanned', {
        cause: 'not_banned_account',
      });
    }

    const resp = await this.prismaService.user.update({
      where: {
        id,
      },
      data: {
        banned: adminBanUserDto.ban,
      },
      select: selectUserFragment,
    });

    return {
      ...resp,
      role: resp.role.name,
    };
  }

  async changeUserPassword(
    id: string,
    adminChangeUserPassword: AdminChangeUserPasswordDTO,
  ): Promise<void> {
    const hashedPassword = await this.scryptService.createHashedFromString(
      adminChangeUserPassword.password,
    );
    const user = await this.prismaService.user.update({
      where: {
        id,
      },
      data: {
        password: hashedPassword,
      },
      select: {
        username: true,
      },
    });
    await this._sendChangePasswordEmail(
      user.username,
      adminChangeUserPassword.password,
    );
  }

  private async _sendChangePasswordEmail(to: string, newPassword: string) {
    const bufferFile = await fs.promises.readFile(
      path.format({ root: 'src', base: '/templates/changePassword.html' }),
    );

    const file = bufferFile.toString();

    const formatHtml = file.replaceAll('[NEW_PASSWORD]', newPassword);
    await this.sendgridService.send({
      to,
      subject: 'reset your password',
      from: '20120551@student.hcmus.edu.vn',
      html: formatHtml,
    });
  }
}
