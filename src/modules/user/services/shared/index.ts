import { BadRequestException, Inject } from '@nestjs/common';
import { IScryptService } from '@roomee1/common';
import { PrismaService } from '@roomee1/prisma';
import { UserResponse } from 'modules/auth/resources/response';
import { SharedCreateUserDTO } from 'modules/user/resources/request';
import { selectUserFragment } from 'types/fragment';
import CryptoJS from 'crypto-js';

export interface IAdminOwnerSharedService {
  createUser(
    userId: string,
    createUserDto: SharedCreateUserDTO,
  ): Promise<UserResponse>;
}
export abstract class AdminOwnerSharedService
  implements IAdminOwnerSharedService
{
  constructor(
    protected prismaService: PrismaService,
    @Inject(IScryptService)
    protected scryptService: IScryptService,
  ) {}

  async createUser(
    userId: string,
    createUserDto: SharedCreateUserDTO,
  ): Promise<UserResponse> {
    const _user = await this.prismaService.user.findUnique({
      where: {
        username: createUserDto.username,
      },
      select: null,
    });

    if (_user) {
      throw new BadRequestException('user has existed', {
        cause: 'user_existed',
      });
    }

    if (!this.isAllowCreatedAccount(createUserDto.role)) {
      throw new BadRequestException(
        'cannot have permission to create this role',
        {
          cause: '',
        },
      );
    }

    const role = await this.prismaService.role.findUnique({
      where: {
        name: createUserDto.role,
      },
      select: {
        id: true,
      },
    });

    if (!role) {
      throw new BadRequestException('not found role', {
        cause: 'not_supported_role',
      });
    }

    const imageUrl = `https://gravatar.com/${CryptoJS.SHA256(
      createUserDto.username,
    )}`;
    const hashedPassword = await this.scryptService.createHashedFromString(
      createUserDto.password,
    );
    const user = await this.prismaService.user.create({
      data: {
        username: createUserDto.username,
        password: hashedPassword,
        firstName: createUserDto.firstName,
        lastName: createUserDto.lastName,
        phoneNumber: '',
        imageUrl,
        roleId: role.id,
        createdBy: userId,
      },
      select: selectUserFragment,
    });

    return {
      ...user,
      role: user.role.name,
    };
  }

  abstract isAllowCreatedAccount(role: string): boolean;
}
