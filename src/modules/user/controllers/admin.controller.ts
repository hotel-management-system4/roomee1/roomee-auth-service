import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiHeader,
  ApiTags,
} from '@nestjs/swagger';
import { UseAuthorized } from 'guards/role.guard';
import { SupportedRole } from 'configurations/role';
import { IAdminService } from '../services';
import {
  AdminBanUserDTO,
  AdminChangeUserPasswordDTO,
  AdminChangeUserRoleDTO,
  SharedCreateUserDTO,
  AdminForceLogoutUserDTO,
  FilterDTO,
} from '../resources/request';
import { UserResponse } from '../resources/response';
import { User } from 'utils/decorator';
import { UserData } from 'types';

@ApiTags('ADMIN API')
@ApiBearerAuth()
@ApiHeader({
  name: 'lang',
  description: 'language',
})
@UseAuthorized({ roles: [SupportedRole.ADMIN] })
@Controller('/api/admin')
export class AdminController {
  constructor(
    @Inject(IAdminService) private readonly _adminService: IAdminService,
  ) {}

  @ApiCreatedResponse({ type: [UserResponse] })
  @HttpCode(HttpStatus.OK)
  @Get('users')
  async getUsers(@Query() filterDto: FilterDTO) {
    const resp = await this._adminService.getUsers(filterDto);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Get('user/:id')
  async getUser(@Param('id') id: string) {
    const resp = await this._adminService.getUser(id);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Post('user')
  async createUser(
    @User() user: UserData,
    @Body() createUserDto: SharedCreateUserDTO,
  ) {
    const resp = await this._adminService.createUser(user.id, createUserDto);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Put('user/:id')
  async changeUserRole(
    @Param('id') id: string,
    @Body() adminChangeUserRoleDto: AdminChangeUserRoleDTO,
  ) {
    const userResponse = await this._adminService.changeUserRole(
      id,
      adminChangeUserRoleDto,
    );
    return userResponse;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Put('user/:id/ban')
  async banUser(
    @Param('id') id: string,
    @Body() adminBanUserDto: AdminBanUserDTO,
  ) {
    const userResponse = await this._adminService.banUser(id, adminBanUserDto);
    return userResponse;
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Put('user/:id/change-password')
  async changeUserPassword(
    @Param('id') id: string,
    @Body() adminChangeUSerPasswordDto: AdminChangeUserPasswordDTO,
  ) {
    await this._adminService.changeUserPassword(id, adminChangeUSerPasswordDto);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Put('user/device/logout')
  async forceLogout(@Body() adminForceLogoutUserDto: AdminForceLogoutUserDTO) {
    await this._adminService.forceLogout(adminForceLogoutUserDto);
  }
}
