import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiHeader,
  ApiTags,
} from '@nestjs/swagger';
import { UseAuthorized } from 'guards/role.guard';
import { SupportedRole } from 'configurations/role';
import {
  SharedCreateUserDTO,
  FilterDTO,
  OwnerToggleLockUserDTO,
} from '../resources/request';
import { UserResponse } from '../resources/response';
import { User } from 'utils/decorator';
import { UserData } from 'types';
import { IOwnerService } from '../services/owner.service';

@ApiTags('OWNER API')
@ApiBearerAuth()
@ApiHeader({
  name: 'lang',
  description: 'language',
})
@UseAuthorized({ roles: [SupportedRole.OWNER] })
@Controller('/api/owner')
export class OwnerController {
  constructor(
    @Inject(IOwnerService) private readonly _ownerService: IOwnerService,
  ) {}

  @ApiCreatedResponse({ type: [UserResponse] })
  @HttpCode(HttpStatus.OK)
  @Get('users')
  async getUsers(@User() user: UserData, @Query() filterDto: FilterDTO) {
    const resp = await this._ownerService.getUsers(user.id, filterDto);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Get('user/:managerId')
  async getUser(@Param('managerId') managerId: string, @User() user: UserData) {
    const resp = await this._ownerService.getUser(user.id, managerId);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Post('user')
  async createUser(
    @User() user: UserData,
    @Body() createUserDto: SharedCreateUserDTO,
  ) {
    const resp = await this._ownerService.createUser(user.id, createUserDto);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Put('user/:id/lock')
  async banUser(
    @Param('id') id: string,
    @Body() ownerLockUserDTO: OwnerToggleLockUserDTO,
  ) {
    const userResponse = await this._ownerService.lockUser(
      id,
      ownerLockUserDTO,
    );
    return userResponse;
  }
}
