import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  Post,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiHeader,
  ApiTags,
} from '@nestjs/swagger';
import { Tracking, User } from 'utils/decorator';
import { TrackingData, UserData } from 'types';
import { IUserService } from '../services';
import { TrackingResponse, UserResponse } from '../resources/response';
import {
  ChangePasswordDTO,
  UpdateUserProfileDTO,
  VerifyDTO,
} from '../resources/request';
import { JwtAccessGuard } from 'guards/jwt.access.guard';

@ApiTags('USER API')
@ApiBearerAuth()
@ApiHeader({
  name: 'lang',
  description: 'language',
})
@UseGuards(JwtAccessGuard)
@Controller('/api/user')
export class UserController {
  constructor(
    @Inject(IUserService) private readonly _userService: IUserService,
  ) {}

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Get()
  async profile(@User() user: UserData) {
    const resp = await this._userService.getUserProfile(user);
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @HttpCode(HttpStatus.OK)
  @Post()
  async updateProfile(
    @Body() updateUserProfileDto: UpdateUserProfileDTO,
    @User() user: UserData,
  ) {
    const resp = await this._userService.updateUserProfile(
      user,
      updateUserProfileDto,
    );
    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @ApiHeader({
    name: 'tracking-id',
    description: 'Tracking id which was created at the same time sending email',
  })
  @HttpCode(HttpStatus.OK)
  @Put('/verify-email')
  async verifyEmail(
    @Body() verifyEmailDto: VerifyDTO,
    @User() user: UserData,
    @Tracking() tracking: TrackingData,
  ) {
    const resp = await this._userService.verifyEmail(
      user,
      tracking.trackingId,
      verifyEmailDto,
    );

    return resp;
  }

  @ApiCreatedResponse({ type: TrackingResponse })
  @HttpCode(HttpStatus.OK)
  @Post('/send-verify-email')
  async sendVerifyEmail(@User() user: UserData) {
    const resp = await this._userService.sendVerifyEmailOTP(user);
    return resp;
  }

  @ApiCreatedResponse({ type: TrackingResponse })
  @ApiHeader({
    name: 'tracking-id',
    description: 'Tracking id which was created at the same time sending email',
  })
  @HttpCode(HttpStatus.OK)
  @Post('/resend-verify-email')
  async resendVerifyEmail(
    @User() user: UserData,
    @Tracking() tracking: TrackingData,
  ) {
    const resp = await this._userService.resendVerifyEmailOTP(
      user,
      tracking.trackingId,
    );

    return resp;
  }

  @ApiCreatedResponse({ type: UserResponse })
  @ApiHeader({
    name: 'tracking-id',
    description: 'Tracking id which was created at the same time sending otp',
  })
  @HttpCode(HttpStatus.OK)
  @Put('/verify-phone')
  async verifyPhone(
    @Body() verifyPhoneDto: VerifyDTO,
    @User() user: UserData,
    @Tracking() tracking: TrackingData,
  ) {
    const resp = await this._userService.verifyPhoneNumber(
      user,
      tracking.trackingId,
      verifyPhoneDto,
    );

    return resp;
  }

  @ApiCreatedResponse({ type: TrackingResponse })
  @HttpCode(HttpStatus.OK)
  @Post('/send-verify-phone')
  async sendVerifyPhone(@User() user: UserData) {
    const resp = await this._userService.sendPhoneNumberOTP(user);

    return resp;
  }

  @ApiCreatedResponse({ type: TrackingResponse })
  @ApiHeader({
    name: 'tracking-id',
    description: 'Tracking id which was created at the same time sending email',
  })
  @HttpCode(HttpStatus.OK)
  @Post('/resend-verify-phone')
  async resendVerifyPhone(
    @User() user: UserData,
    @Tracking() tracking: TrackingData,
  ) {
    const userResponse = await this._userService.resendPhoneNumberOTP(
      user,
      tracking.trackingId,
    );

    return userResponse;
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('/change-password')
  async changePassword(
    @Body() changePasswordDto: ChangePasswordDTO,
    @User() user: UserData,
  ) {
    await this._userService.changePassword(user, changePasswordDto);
  }

  @ApiCreatedResponse({ type: UserResponse })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @HttpCode(HttpStatus.OK)
  @Post('/avatar')
  @UseInterceptors(FileInterceptor('file'))
  async uploadAvatar(
    @UploadedFile() file: Express.Multer.File,
    @User() user: UserData,
  ) {
    const payload = {
      filename: file.originalname,
      buffer: file.buffer,
      mimeType: file.mimetype,
    };

    const userResponse = await this._userService.updateUserAvatar(
      user,
      payload,
    );

    return userResponse;
  }
}
