import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FirebaseModule, FirebaseModuleOptions } from '@roomee1/firebase';
import {
  AdminController,
  OwnerController,
  UserController,
} from './controllers';
import {
  UserService,
  IUserService,
  IAdminService,
  AdminService,
} from './services';
import { IOwnerService, OwnerService } from './services/owner.service';

@Module({
  imports: [
    FirebaseModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const firebase = configService.get<FirebaseModuleOptions>('firebase');
        return firebase;
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [UserController, AdminController, OwnerController],
  providers: [
    {
      provide: IUserService,
      useClass: UserService,
    },
    {
      provide: IAdminService,
      useClass: AdminService,
    },
    {
      provide: IOwnerService,
      useClass: OwnerService,
    },
  ],
})
export class UserModule {}
