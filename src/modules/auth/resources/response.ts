import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

export class DeviceResponse {
  @ApiProperty({
    type: String,
    default: '0969-xs-assa486904',
  })
  deviceId: string;

  @ApiProperty({
    type: String,
    default: 'chrome',
  })
  deviceName?: string;
}
export class UserResponse {
  @ApiProperty({
    type: String,
    default: 'MyUsername',
  })
  username: string;

  @ApiProperty({
    type: String,
    default: 'Andrew',
  })
  firstName: string;

  @ApiProperty({
    type: String,
    default: 'NG',
  })
  lastName: string;

  @ApiProperty({
    type: String,
    default: '223232a',
  })
  id: string;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  locked: boolean;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  emailVerified: boolean;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  phoneVerified: boolean;

  @ApiProperty({
    type: String,
    default: '0969486904',
  })
  phoneNumber: string;

  @ApiProperty({
    type: String,
    default: 'http://google.com/123.png',
  })
  imageUrl: string;

  @ApiProperty({
    type: String,
    default: 'TENANT',
  })
  role: string;
}

export class TokenResponse {
  @ApiProperty({
    type: String,
    default:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoiTXlVc2VybmFtZSIsImZpcnN0TmFtZSI6IkFuZHJldyIsImxhc3ROYW1lIjoiTmd1eWVuIiwiaXNBY3RpdmF0ZWQiOmZhbHNlLCJpYXQiOjE3MDU3Mzk3NTcsImV4cCI6MTcwNTc0MzM1NywiaXNzIjoiUm9hZFRvR3JhZHVhdGlvbiIsInN1YiI6IlJlZnJlc2gifQ.KiaY3PE9WkrNYOem5694cdHjo4MqlrUZ57ROP5_zrtyABTa1LRgcPvlOoDkS0tF3tErepLP87QZd6Uu-LOXqWVtS-CCGksqzZp23rgfZ7lsNw7M0ujidjRDWodKZlukg8A9pBVfVKKUCqOwXiY72RNiC_fg5Qd5ZZWmP5tMkU-W61SB88pr-3BNU2Y-Lcvuha8lV1l7eXWFvYwBfJjezVbeobllVILOouSw4YJLG4XffzYeSWgBcRBFzTcsOTyRkKitIcZ6kV324Lxd38mZfXUfLUafP1efmI8yfaXbn9wh9Md39syVJGXSFzwQrXX22zHLN70qLKIWYkQv9692HaQ',
  })
  accessToken: string;

  @ApiProperty({
    type: String,
    default:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoiTXlVc2VybmFtZSIsImZpcnN0TmFtZSI6IkFuZHJldyIsImxhc3ROYW1lIjoiTmd1eWVuIiwiaXNBY3RpdmF0ZWQiOmZhbHNlLCJpYXQiOjE3MDU3Mzk3NTcsImV4cCI6MTcwNTc0MzM1NywiaXNzIjoiUm9hZFRvR3JhZHVhdGlvbiIsInN1YiI6IlJlZnJlc2gifQ.KiaY3PE9WkrNYOem5694cdHjo4MqlrUZ57ROP5_zrtyABTa1LRgcPvlOoDkS0tF3tErepLP87QZd6Uu-LOXqWVtS-CCGksqzZp23rgfZ7lsNw7M0ujidjRDWodKZlukg8A9pBVfVKKUCqOwXiY72RNiC_fg5Qd5ZZWmP5tMkU-W61SB88pr-3BNU2Y-Lcvuha8lV1l7eXWFvYwBfJjezVbeobllVILOouSw4YJLG4XffzYeSWgBcRBFzTcsOTyRkKitIcZ6kV324Lxd38mZfXUfLUafP1efmI8yfaXbn9wh9Md39syVJGXSFzwQrXX22zHLN70qLKIWYkQv9692HaQ',
  })
  refreshToken?: string;
  @ApiProperty({
    type: Number,
    default: 3600,
  })
  expiresIn: number;
}

export class TrackingResponse {
  @ApiProperty({
    type: String,
    default: 'uuid',
  })
  trackingId: string;
}

export class UserTokenResponse {
  @ApiProperty({
    type: UserResponse,
  })
  @Expose()
  user: UserResponse;

  @ApiProperty({
    type: TokenResponse,
  })
  @Expose()
  token: TokenResponse;

  @ApiProperty({
    type: TrackingResponse,
  })
  @Expose()
  tracking?: TrackingResponse;

  @ApiProperty({
    type: DeviceResponse,
  })
  @Expose()
  device: DeviceResponse;
}
