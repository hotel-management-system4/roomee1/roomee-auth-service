import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsPhoneNumber,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { RoleList } from '@prisma/client';
import {
  i18nIsEmail,
  i18nIsNotEmpty,
  i18nIsNumber,
  i18nIsPhoneNumber,
  i18nIsString,
} from 'utils/i18n.validation';

export class ForgotPasswordDTO {
  @ApiProperty({
    default: 'myUsername',
  })
  @IsString(i18nIsString)
  username: string;
}

export class UpdatePasswordDTO {
  @ApiProperty({
    default: 'myNewPassword',
  })
  @IsNotEmpty(i18nIsNotEmpty)
  password: string;
}

export class VerifyCodeDTO {
  @ApiProperty({
    default: 1234,
    description: 'Please check your email to get the verification code',
  })
  @IsNumber({ maxDecimalPlaces: 4 }, i18nIsNumber)
  verifyCode: number;

  @ApiProperty({
    default: 'myemail@gmail.com',
    description: 'Email verification',
  })
  @IsEmail({}, i18nIsEmail)
  email: string;
}

export class MailConfirmDTO {
  @ApiProperty({
    default: 'Your Token',
  })
  @IsString(i18nIsString)
  token: string;
}

export class ResendEmailConfirmationDTO {
  @ApiProperty({
    default: 'youemail@email.com',
  })
  @IsEmail({}, i18nIsEmail)
  email: string;
}

export class SignInDTO {
  @ApiProperty({
    default: 'MyUsername',
  })
  @IsString(i18nIsString)
  username: string;

  @ApiProperty({
    type: String,
    default: 'MyPassword@1',
  })
  @IsString(i18nIsString)
  password: string;
}

export class SignUpDTO {
  @ApiProperty({
    default: 'phuc@gmail.com',
  })
  @IsEmail({}, i18nIsEmail)
  username: string;

  @ApiProperty({
    default: 'Andrew',
  })
  @IsString(i18nIsString)
  firstName: string;

  @ApiProperty({
    type: String,
    default: 'Nguyen',
  })
  @IsString(i18nIsString)
  lastName: string;

  @ApiProperty({
    type: String,
    default: 'MyPassword@1',
  })
  @IsNotEmpty(i18nIsNotEmpty)
  password: string;

  @ApiProperty({
    type: String,
    default: '0969486907',
  })
  @IsPhoneNumber('VN', i18nIsPhoneNumber)
  @IsString()
  phoneNumber: string;

  @ApiProperty({
    default: 'TENANT',
    type: String,
  })
  @IsString(i18nIsString)
  role: RoleList;
}
