import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Inject,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiHeader,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAccessGuard } from 'guards/jwt.access.guard';
import { JwtRefreshGuard } from 'guards/jwt.refresh.guard';
import { Device, Tracking, User } from 'utils/decorator';
import { DeviceData, TrackingData, UserData } from 'types';
import { IAuthService } from '../services';
import {
  ForgotPasswordDTO,
  SignInDTO,
  SignUpDTO,
  UpdatePasswordDTO,
  VerifyCodeDTO,
} from '../resources/request';
import { TrackingResponse, UserTokenResponse } from '../resources/response';

@ApiTags('JWT Auth')
@ApiHeader({
  name: 'lang',
  description: 'language',
})
@Controller('/api/auth')
export class AuthController {
  constructor(
    @Inject(IAuthService)
    private readonly authService: IAuthService,
  ) {}

  @ApiCreatedResponse({ type: UserTokenResponse })
  @HttpCode(HttpStatus.OK)
  @Post('/sign-up')
  async signUp(@Body() signUpDto: SignUpDTO, @Device() device: DeviceData) {
    const result = await this.authService.signUp(signUpDto, device);
    return result;
  }

  @ApiCreatedResponse({ type: UserTokenResponse })
  @HttpCode(HttpStatus.OK)
  @Post('/sign-in')
  async signIn(@Body() signInDto: SignInDTO, @Device() device: DeviceData) {
    const result = await this.authService.signIn(signInDto, device);
    return result;
  }

  @ApiBearerAuth()
  @UseGuards(JwtAccessGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('/sign-out')
  async signOut(@User() user: UserData, @Device() device: DeviceData) {
    await this.authService.signOut(user.id, device);
  }

  @ApiBearerAuth()
  @ApiHeader({
    name: 'device-id',
    description:
      'Each device/browser has a unique device id that was created and included in the sign in request',
  })
  @UseGuards(JwtRefreshGuard)
  @Post('refresh')
  async refresh(@User() user: UserData, @Device() device: DeviceData) {
    const result = await this.authService.refreshToken(user.id, device);
    return result;
  }

  @ApiCreatedResponse({ type: TrackingResponse })
  @Post('forgot-password')
  @HttpCode(HttpStatus.OK)
  async forgetPassword(@Body() forgetPasswordDto: ForgotPasswordDTO) {
    const result = await this.authService.forgotPassword(forgetPasswordDto);
    return result;
  }

  @ApiCreatedResponse({ type: TrackingResponse })
  @ApiHeader({
    name: 'tracking-id',
    description: 'Tracking id which was created at the same time sending email',
  })
  @Post('verify-code')
  @HttpCode(HttpStatus.OK)
  async verifyCode(
    @Body() verifyCodeDto: VerifyCodeDTO,
    @Tracking() tracking: TrackingData,
  ) {
    const result = await this.authService.verifyCode(
      tracking.trackingId,
      verifyCodeDto,
    );
    return result;
  }

  @ApiHeader({
    name: 'tracking-id',
    description: 'Tracking id which was created at the same time otp success',
  })
  @Put('update-password')
  @HttpCode(HttpStatus.OK)
  async updatePassword(
    @Body() updatePasswordDto: UpdatePasswordDTO,
    @Tracking() tracking: TrackingData,
  ) {
    await this.authService.updatePassword(
      tracking.trackingId,
      updatePasswordDto,
    );
  }
}
