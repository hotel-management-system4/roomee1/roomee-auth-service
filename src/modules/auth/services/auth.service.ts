import fs from 'fs';
import path from 'path';
import sha256 from 'crypto-js/sha256';
import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { RoleList } from '@prisma/client';
import { ConfigService } from '@nestjs/config';
import { PrismaService } from '@roomee1/prisma';
import {
  BadRequestException,
  IRedisCacheService,
  IScryptService,
  UnauthorizedException,
} from '@roomee1/common';
import { ISendgridService } from '@roomee1/sendgrid';
import { randomInt, randomUUID } from 'crypto';
import { DeviceData, TokenOptions, UserData } from 'types';
import { selectUserFragment } from 'types/fragment';
import {
  ForgotPasswordDTO,
  SignInDTO,
  SignUpDTO,
  UpdatePasswordDTO,
  VerifyCodeDTO,
} from '../resources/request';
import {
  TokenResponse,
  TrackingResponse,
  UserTokenResponse,
} from '../resources/response';

export const IAuthService = 'IAuthService';
export interface IAuthService {
  signUp(signUpDto: SignUpDTO, device: DeviceData): Promise<UserTokenResponse>;
  signIn(signInDto: SignInDTO, device: DeviceData): Promise<UserTokenResponse>;
  signOut(userId: string, device: DeviceData): Promise<void>;
  refreshToken(userId: string, device: DeviceData): Promise<TokenResponse>;
  updatePassword(
    trackingId: string,
    updatePasswordDto: UpdatePasswordDTO,
  ): Promise<void>;
  verifyCode(
    trackingId: string,
    verifyCodeDto: VerifyCodeDTO,
  ): Promise<TrackingResponse>;
  forgotPassword(
    forgotPasswordDto: ForgotPasswordDTO,
  ): Promise<TrackingResponse>;
}
@Injectable()
export class AuthService implements IAuthService {
  private readonly accessTokenOptions: TokenOptions;
  private readonly refreshTokenOptions: TokenOptions;

  constructor(
    configService: ConfigService,
    private prismaService: PrismaService,
    private jwtService: JwtService,
    @Inject(IRedisCacheService)
    private cacheService: IRedisCacheService,
    @Inject(IScryptService)
    private hashService: IScryptService,
    @Inject(ISendgridService)
    private sendgridService: ISendgridService,
  ) {
    this.accessTokenOptions = configService.get<TokenOptions>('accessToken');
    this.refreshTokenOptions = configService.get<TokenOptions>('refreshToken');
  }

  async signUp(
    signUpDto: SignUpDTO,
    device: DeviceData,
  ): Promise<UserTokenResponse> {
    const { username, password } = signUpDto;
    const user = await this.prismaService.user.findUnique({
      where: {
        username,
      },
      select: null,
    });

    if (user) {
      throw new BadRequestException('Username already existed', {
        cause: 'user_existed',
      });
    }

    const role = await this.prismaService.role.findUnique({
      where: {
        name: signUpDto.role,
      },
      select: {
        id: true,
      },
    });

    const notFoundRoleOrNotAcceptedRole =
      !role ||
      signUpDto.role === RoleList.ADMIN ||
      signUpDto.role === RoleList.MOD ||
      signUpDto.role === RoleList.MANAGER;

    if (notFoundRoleOrNotAcceptedRole) {
      throw new BadRequestException('Not supported role', {
        cause: 'not_supported_role',
      });
    }

    const hashedPassword =
      await this.hashService.createHashedFromString(password);

    // TODO: getImageUrl from email address
    const imageUrl = `https://gravatar.com/avatar/${sha256(username)}`;

    // get image url from email
    const userCreated = await this.prismaService.user.create({
      data: {
        username: signUpDto.username,
        password: hashedPassword,
        firstName: signUpDto.firstName || '',
        lastName: signUpDto.lastName || '',
        phoneNumber: signUpDto.phoneNumber,
        imageUrl,
        roleId: role.id,
      },
      select: selectUserFragment,
    });

    const deviceId = this.hashService.generateRandomId();
    const payload = { ...userCreated, role: signUpDto.role };
    const { token } = await this._createAndCacheTokens(
      { deviceName: device.deviceName, deviceId },
      payload,
    );

    const { otp, trackingId } = this._randomOTP(4);
    await this.cacheService.set(
      `${userCreated.id}:tracking:${trackingId}`,
      otp,
      {
        EX: 300,
      },
    );

    await this._sendVerifyEmail(userCreated.username, otp);

    return {
      user: {
        ...payload,
      },
      token,
      tracking: { trackingId },
      device: {
        deviceId,
        deviceName: device.deviceName,
      },
    };
  }

  async signIn(
    signInDto: SignInDTO,
    device: DeviceData,
  ): Promise<UserTokenResponse> {
    const { password, username } = signInDto;
    const user = await this.prismaService.user.findUnique({
      where: {
        username,
      },
      include: {
        role: true,
      },
    });

    if (!user) {
      throw new UnauthorizedException('Invalid username', {
        cause: 'not_existed_user',
      });
    }

    const { banned, password: userPassword, role, ...payload } = user;

    const [salt, storedHash] = this.hashService.getSalt(userPassword, -64);
    const hash = await this.hashService.createHash(password, salt);

    if (storedHash !== hash.toString('hex')) {
      throw new UnauthorizedException('Wrong password', {
        cause: 'wrong_password',
      });
    }

    const deviceId = this.hashService.generateRandomId();
    const tokenPayload = {
      ...payload,
      role: role.name,
      banned,
    };

    const { token } = await this._createAndCacheTokens(
      { deviceId, deviceName: device.deviceName },
      tokenPayload,
    );

    if (!user.emailVerified) {
      const { otp, trackingId } = this._randomOTP(4);
      await this.cacheService.set(`${user.id}:tracking:${trackingId}`, otp, {
        EX: 300,
      });

      await this._sendVerifyEmail(user.username, otp);
    }

    return {
      token,
      user: {
        ...tokenPayload,
      },
      device: {
        deviceId,
        deviceName: device.deviceName,
      },
    };
  }

  async signOut(userId: string, device: DeviceData): Promise<void> {
    await this.cacheService.del(
      `${userId}:device:${device.deviceId}:${device.deviceName}`,
    );
  }

  async refreshToken(
    userId: string,
    device: DeviceData,
  ): Promise<TokenResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
      select: selectUserFragment,
    });

    const tokenPayload = {
      ...user,
      role: user.role.name,
    };

    await this.cacheService.del(
      `${userId}:device:${device.deviceId}:${device.deviceName}`,
    );
    const { token } = await this._createAndCacheTokens(device, tokenPayload);
    return token;
  }

  async forgotPassword(
    forgotPasswordDto: ForgotPasswordDTO,
  ): Promise<TrackingResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        username: forgotPasswordDto.username,
      },
      select: {
        id: true,
      },
    });
    if (!user) {
      throw new BadRequestException('Username not existed', {
        cause: 'user_existed',
      });
    }

    const { trackingId, otp } = this._randomOTP(4);
    const redisKey = `${user.id}:tracking:${trackingId}`;
    await this.cacheService.set(redisKey, otp, {
      EX: 300,
    });

    await this._sendVerifyEmail(forgotPasswordDto.username, otp);

    return {
      trackingId,
    };
  }

  async verifyCode(
    trackingId: string,
    verifyCodeDto: VerifyCodeDTO,
  ): Promise<TrackingResponse> {
    const user = await this.prismaService.user.findUnique({
      where: {
        username: verifyCodeDto.email,
      },
      select: {
        id: true,
      },
    });

    if (!user) {
      throw new BadRequestException('Username not existed', {
        cause: 'not_existed_user',
      });
    }
    const redisKey = `${user.id}:tracking:${trackingId}`;
    const code = await this.cacheService.get<string>(redisKey);

    if (parseInt(code) !== verifyCodeDto.verifyCode) {
      throw new BadRequestException('Wrong verifcation code', {
        cause: 'wrong_otp',
      });
    }

    await this.cacheService.del(redisKey);
    const newTrackingId = randomUUID();

    await this.cacheService.set(newTrackingId, user.id, { EX: 300 });
    return {
      trackingId: newTrackingId,
    };
  }

  async updatePassword(
    trackingId: string,
    updatePasswordDto: UpdatePasswordDTO,
  ): Promise<void> {
    const userId = await this.cacheService.get<string>(trackingId);
    if (!userId) {
      throw new BadRequestException('wrong signature to update password', {
        cause: 'wrong_signature',
      });
    }

    const hashedPassword = await this.hashService.createHashedFromString(
      updatePasswordDto.password,
    );
    await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        password: hashedPassword,
      },
    });
    await this.cacheService.del(trackingId);
  }

  async cacheRefreshToken(
    device: DeviceData,
    userId: string,
    refreshToken: string,
  ): Promise<string> {
    const key = `${userId}:device:${device.deviceId}:${device.deviceName}`;
    const res = await this.cacheService.set(key, refreshToken, {
      EX: this.refreshTokenOptions.expireTime,
    });
    return res;
  }

  private async _createAndCacheTokens(device: DeviceData, user: UserData) {
    const token = this._createTokens(user);
    //cached the refresh token
    await this.cacheRefreshToken(device, user.id, token.refreshToken);
    return {
      token,
    };
  }

  private _createTokens(user: UserData): TokenResponse {
    const { accessToken, expiresIn } = this._createAccessToken(user);
    const { refreshToken } = this._createRefreshToken(user);
    return { accessToken, refreshToken, expiresIn };
  }

  private _createAccessToken(user: UserData) {
    const payload = user;
    const accessToken = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Refresh',
      expiresIn: this.accessTokenOptions.expireTime,
      algorithm: 'RS256',
      privateKey: this.accessTokenOptions.privateKey,
    });

    return {
      accessToken,
      expiresIn: this.accessTokenOptions.expireTime,
    };
  }

  private _createRefreshToken(user: UserData) {
    const payload = user;
    const refreshToken = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Refresh',
      expiresIn: this.refreshTokenOptions.expireTime,
      algorithm: 'RS256',
      privateKey: this.refreshTokenOptions.privateKey,
    });

    return {
      refreshToken,
      expiresIn: this.refreshTokenOptions.expireTime,
    };
  }

  private _randomOTP(len: number) {
    const randomCode = randomInt(
      Math.pow(10, len - 1),
      9 * Math.pow(10, len - 1),
    );
    const trackingId = randomUUID();
    return {
      otp: randomCode,
      trackingId,
    };
  }

  private async _sendVerifyEmail(to: string, otp: number) {
    const bufferFile = await fs.promises.readFile(
      path.format({ root: 'src', base: '/templates/verifyEmail.html' }),
    );

    const file = bufferFile.toString();

    const formatHtml = file.replaceAll('[OTP]', otp.toString());
    await this.sendgridService.send({
      to,
      subject: 'verify your account',
      from: '20120551@student.hcmus.edu.vn',
      html: formatHtml,
    });
  }
}
