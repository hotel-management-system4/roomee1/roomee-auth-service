import { i18nValidationMessage } from 'nestjs-i18n';

export const i18nIsString = {
  message: i18nValidationMessage('validation.is_string'),
};
export const i18nIsEmail = {
  message: i18nValidationMessage('validation.is_email'),
};
export const i18nIsNotEmpty = {
  message: i18nValidationMessage('validation.is_not_empty'),
};
export const i18nIsNumber = {
  message: i18nValidationMessage('validation.is_number'),
};
export const i18nIsBoolean = {
  message: i18nValidationMessage('validation.is_phone_number'),
};
export const i18nIsPhoneNumber = {
  message: i18nValidationMessage('validation.is_phone_number'),
};
