import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { setupSwagger, validationError } from './bootstrap';
import { DomainExceptionFilter } from './middlewares';
import { CommonLoggerService } from '@roomee1/common';
import { I18nValidationExceptionFilter, I18nValidationPipe } from 'nestjs-i18n';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  const logger = await app.resolve(CommonLoggerService);
  app.useLogger(logger);
  app.useGlobalFilters(
    new DomainExceptionFilter(logger),
    new I18nValidationExceptionFilter(),
  );
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: validationError,
    }),
  );

  setupSwagger(app);
  await app.listen(3000);
}
bootstrap();
