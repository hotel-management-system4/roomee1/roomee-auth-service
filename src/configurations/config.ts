import { registerAs } from '@nestjs/config';
import { env } from 'process';
import {
  accessTokenPrivateKey,
  accessTokenPublicKey,
} from './keys/accessTokenKey';
import {
  refreshTokenPrivateKey,
  refreshTokenPublicKey,
} from './keys/refreshTokenKey';

export const accessToken = registerAs('accessToken', () => ({
  privateKey: accessTokenPrivateKey,
  publicKey: accessTokenPublicKey,
  expireTime: 3600,
}));

export const refreshToken = registerAs('refreshToken', () => ({
  privateKey: refreshTokenPrivateKey,
  publicKey: refreshTokenPublicKey,
  expireTime: 86400,
}));

export const facebook = registerAs('facebook', () => ({
  clientID: env.FACEBOOK_CLIENT_ID,
  appSecret: env.FACEBOOK_APP_SECRET,
  callbackUrl: env.FACEBOOK_CALL_BACK_URL,
  scope: 'email',
  profileFields: ['emails', 'name'],
}));

export const google = registerAs('google', () => ({
  clientID: env.GOOGLE_CLIENT_ID,
  appSecret: env.GOOGLE_APP_SECRET,
  callbackUrl: env.GOOGLE_CALL_BACK_URL,
}));

export const cache = registerAs('cache', () => ({
  url: env.CACHE_QUEUE_URL,
}));

export const firebase = registerAs('firebase', () => ({
  apiKey: env.FIREBASE_API_KEY,
  authDomain: env.FIREBASE_AUTH_DOMAIN,
  projectId: env.FIREBASE_PROJECT_ID,
  storageBucket: env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: env.FIREBASE_MESSAGE_SENDER_ID,
  appId: env.FIREBASE_APP_ID,
  measurementId: env.FIREBASE_MEMSUREMENT_ID,
}));

export const sendgrid = registerAs('sendgrid', () => ({
  apiKey: env.SENDGRID_API_KEY,
}));
